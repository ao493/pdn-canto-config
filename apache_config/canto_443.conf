<IfModule mod_ssl.c>
	<VirtualHost _default_:443>

		ServerAdmin service-desk@uis.cam.ac.uk
		ServerName flybase-vm.pdn.cam.ac.uk

		# Log locations
		ErrorLog ${APACHE_LOG_DIR}/error.log
		CustomLog ${APACHE_LOG_DIR}/access.log combined

		# Enable TLS
		SSLEngine on

		# TLS cert details
		SSLCertificateFile /etc/letsencrypt/live/flybase-vm.pdn.cam.ac.uk/fullchain.pem	
		SSLCertificateKeyFile /etc/letsencrypt/live/flybase-vm.pdn.cam.ac.uk/privkey.pem 
		Include /etc/letsencrypt/options-ssl-apache.conf

		# cgi-bin config
		<FilesMatch "\.(cgi|shtml|phtml|php)$">
				SSLOptions +StdEnvVars
		</FilesMatch>
		<Directory /usr/lib/cgi-bin>
				SSLOptions +StdEnvVars
		</Directory>

		# Tweak headers
		RequestHeader set X-Forwarded-Port 443
		# Header always set Access-Control-Allow-Origin "*"
  		# Header always set Access-Control-Allow-Methods "OPTIONS, GET"
  		# Header always set Access-Control-Max-Age "5000"
		# Header always set Access-Control-Allow-Headers "x-requested-with, Content-Type, origin, authorization, accept, client-security-token"


		# Configure rewrites
		RewriteEngine On
  		RewriteRule ^/$ /pombe/docs/canto_front [PT,QSA]
  		RewriteRule ^/(docs|tools|local)/(.*) /pombe/$1/$2 [PT,QSA]

		# Define location
		<Location /pombe>
                        RequestHeader set X-Request-Base /pombe
                </Location>

		# Configure Proxy
                ProxyPass /pombe http://localhost:7000 retry=5
                ProxyPassReverse /pombe http://localhost:7000

		<IfModule mod_deflate.c>
    			SetOutputFilter DEFLATE
    			# Don’t compress
    			SetEnvIfNoCase Request_URI \.(?:gif|jpe?g|png)$ no-gzip dont-vary

    			# Deal with proxy servers
    			<IfModule mod_headers.c>
      				Header append Vary User-Agent
    			</IfModule>
  		</IfModule>

	</VirtualHost>
</IfModule>

# vim: syntax=apache ts=4 sw=4 sts=4 sr noet
