#!/bin/bash -

# This script exports data from canto - to be set to run automatically on Wednesdays noon.

# Define canto-space root in flybase-vm
CANTOSPACE="/data/export/canto-project/canto-space"

# Define logs folder inside canto-space
LOGS="logs"

# Define log file
LOGFILE="${CANTOSPACE}/${LOGS}/canto_weekly_export.log"

# Redirect all Terminal/command line output to logfile
exec &>> "${LOGFILE}"

# Function to log messages with date stamp
# e.g. log "Canto script completed successfully" generates something like 2020-05-20 10:24:37: Canto script completed successfully

function log () {
    local DATESTAMP=$(date +"%Y-%m-%d %H:%M:%S")
    /bin/echo "${DATESTAMP}: ${1}"
}

# Define initial export directory filepath
EXPORT_DIR="${CANTOSPACE}/import_export"

#Define export filename
EXPORT_FILENAME=canto_server_export_latest.json

#Define filepath to dedicated archives folder at export-vm
EXPORTVM_EXPORT_ARCHIVE="/data/export/archives/canto_export_json"

log "Starting"

#Export of 'APPROVED' sessions from Canto into json file
#this step also relabels those 'APPROVED' sessions as 'EXPORTED', so that they will not be exported again next time this is executed
if ./canto/script/canto_docker  --non-interactive ./script/canto_export.pl canto-json --export-curator-names --export-approved > "${EXPORT_DIR}/${EXPORT_FILENAME}"; then
	
	log "Approved sessions exported into ${EXPORT_DIR} successfully"
	
	#Define date-time-stamped filename for archiving
	EXPORT_FILENAME_ARCHIVE="canto_server_export_$(date +"%Y-%m-%d").json"
		
    log "Copying ${EXPORT_FILENAME} to "${EXPORTVM_EXPORT_ARCHIVE}" and also archiving it as "${EXPORT_FILENAME_ARCHIVE}""

    #Copying the export file to within flybase-vm's archives, both as the generic 'canto_server_export_latest.json' file, which will be overwritten every week, as a date-stamped version 
    cp "${EXPORT_DIR}/${EXPORT_FILENAME}" "${EXPORTVM_EXPORT_ARCHIVE}/${EXPORT_FILENAME}"
	cp "${EXPORT_DIR}/${EXPORT_FILENAME}" "${EXPORTVM_EXPORT_ARCHIVE}/${EXPORT_FILENAME_ARCHIVE}"
               
	log "Finished"

	exit
					
	fi

else
	
	log "Export failed."
		
	log "Finished"
	
	exit

fi
	
exit
