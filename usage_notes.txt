List all timers
# systemctl list-timers --all

Should see output like
# root@flybase-vm:/data/export/canto-space/logs# systemctl list-timers --all
NEXT                         LEFT        LAST                         PASSED        UNIT                         ACTIVATES
Mon 2020-12-07 01:00:00 GMT  5 days left Mon 2020-11-30 01:00:35 GMT  1 day 10h ago canto_weekly.timer           canto_weekly.service

Basic consistency check of script (no output is good)
# bash -n weekly_routine.sh

