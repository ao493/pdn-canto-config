#scipt(s) to configure extensions for Canto from 'flybase_controlled_vocabulary.obo'

# 1 - to eliminate irrelevant text from the obo file.
sed -n -e '/\[Term\]/,$p' ./import_export/flybase_controlled_vocabulary.obo | perl -pe 's/(^\[Term\])//g' | perl -pe 's/(^\[Typedef\])//g' | perl -pe 's/: /\t/g' > ./extension_config-Rscript/tempFBcvobo.txt


# 2 - this Rscript creates an extension_config.tsv file that defines which qualifiers (extensions in pombase lingo) can go with which type of annotation, specified in 'allowed_qualifiers.tsv'
# The list of allowed qualifiers is an array of namespaces
# The annotation types are defined as the top term IDs for the type of annotation
	# phenotypic class - FBcv:0000347
	# anatomy/manifests uses two ontologies and thus needs two ids:
		# flybase anatomy - FBbt:10000000
		# GO CC - GO:0005575.
# the resulting extension_config.tsv only uses IDs and, therefore, the qualifier namespaces must be converted into topmost IDs of that namespace, whcih requires full processing of the FBcv obo file.
# allowed_qualifiers.tsv or test-script3.R may have to be changed if a) the ontologies change top terms, b) mortality- and fertility-related terms are re-organised, or c) the list of qualifiers changes

Rscript ./extension_config-Rscript/list-to-extension_config.R

rm ./extension_config-Rscript/tempFBcvobo.txt